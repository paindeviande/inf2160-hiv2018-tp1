# Travail pratique 1 - Énumération de sous-arbres induits en Haskell

## Description

Travail pratique dans le cadre du cours de INF2160 - Paradigme de programmation
à la session d'hiver 2018 à l'Université du Québec à Montréal. Dans le cadre de 
ce travail pratique, nous nous intéressons aux sous-graphes
induits qui sont des arbres, appelés sous-arbres induits.

## Auteur

Louis-Philippe Geoffrion (GEOL15129306)

## Fonctionnement

Le module se compile seulement avec les [dépendances](#dependances). Pour se faire,
il suffit d'utiliser la commande:

~~~
make
~~~

Pour exécuter le projet, il suffit de lancer l'exécutable ``./main`` ou la commande:

~~~
make exec
~~~

Suite à l'installation de ``doctest``, il est possible de lancer les tests automatiques
avec la commande:

~~~
make test
~~~

Suite à l'installation de ``haddock``, il est possible de générer automatiquement la
documentation du projet à l'aide de la commande:

~~~
make doc
~~~

Pour choisir le graphe à générer, il suffit de modifier la ligne dans ``Main.hs``
par le graphe souhaité:

~~~
graph = wheelGraph 5
~~~

Pour générer le graphe, il suffit de lancer la commande ``./main`` ou encore:

~~~
make exec
~~~

Pour un graphe de roue à 5 sommets, on obtient donc l'image:

![Wheel Graph](https://gitlab.com/paindeviande/inf2160-hiv2018-tp1/raw/master/img/wheelGraph.png)

## Contenu du projet

*``Main.hs``: Module de gestion de l'exécution des autres modules. Génère le texte pour créer les images.
*``Graph.hs``: Module de gestion des graphes. Permet de créer des graphes prédéfinis, d'ajouter des sommets et des arcs et fournit autres fonctionnalités pour les graphes.
*``Configuration.hs``: Module de gestion des configurations. Une configuration représente un sous-arbre induit d'un graphe.
*``ISTree.hs``: Module de gestion des configurations pour un certain graphe.
*``Makefile``: Fichier Makefile servant à automatiser des tâches du projet.
*``img``: Dossier contenant l'image de référence pour le README.md
*``README.md``: Fichier markdown de référence pour le projet.

## Dépendances

* [Graphviz](https://graphviz.gitlab.io/download/), le générateur d'image à partir de texte (incluant les dépendances).
* [GHC](https://www.haskell.org/ghc/), le compilateur de Haskell.
* [Cabal](https://www.haskell.org/cabal/users-guide/), système de gestion de paquets pour Haskell
* [Doctest](https://github.com/sol/doctest#readme), pour lancer les tests automatiques
* [Haddock](https://www.haskell.org/haddock/), pour générer la documentation Haskell

## Statut

Projet complet.
