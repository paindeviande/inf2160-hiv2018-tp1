{-|
Module      : ISTree
Description : A module for enumerating induced subtrees
Copyright   : (c) Alexandre Blondin Massé
License     : GPL-3
Maintainer  : blondin_masse.alexandre@uqam.ca
Stability   : experimental

This module provides functionalities for enumerating induced subtrees of an
undirected graph. It provides a data structure called `ISTree`, which is a tree
having the property that all of its leaves store a unique induced subtree of
the graph.

It also provides functions enumerating the subtrees and counting them. Finally,
it is possible to generate a Graphviz string of the tree so that each node
contains a configuration.
 -}
module ISTree (
    -- * Type and constructor
    ISTree, buildISTree,
    -- * Queries
    namedConfigurations, allSubtrees, numSubtrees,
    -- * Graphviz output
    toGraphvizString
    ) where

import Data.List (intercalate,sort)
import Data.Maybe (fromJust)
import Data.Foldable (Foldable,foldMap)
import qualified Data.Foldable (foldr)
import Data.Monoid (mempty,mappend)
import Graph hiding (toGraphvizString)
import Configuration hiding (toGraphvizString)
import qualified Configuration (toGraphvizString)

-----------------
-- Constructor --
-----------------

-- | A binary tree containing things
data Tree a = Empty | Leaf a | Branch a (Tree a) (Tree a)
    deriving (Show)

-- | The usual fmap for tree-like structures
instance Functor Tree where
    fmap f Empty = Empty
    fmap f (Leaf a) = Leaf (f a)
    fmap f (Branch a t1 t2) = Branch (f a) (fmap f t1) (fmap f t2)

-- | We fold only the leaves
instance Foldable Tree where
    foldMap f Empty = mempty
    foldMap f (Leaf a) = f a
    foldMap f (Branch a t1 t2) = foldMap f t1 `mappend` foldMap f t2

-- | An ISTree is a binary tree containing configurations
type ISTree v = Tree (Configuration v)

-- | Builds a tree of all configurations of a graph
--
-- >>> show $ buildISTree $ completeGraph 1
-- "Branch Configuration(Included={}, Excluded={}, Free={1}) (Leaf Configuration(Included={1}, Excluded={}, Free={})) (Leaf Configuration(Included={}, Excluded={1}, Free={}))"
buildISTree :: Ord v => Graph v -> ISTree v
buildISTree g = subTreeLoop c
        where c = initConfiguration g
              subTreeLoop c1 = if extendable c1 then Branch c1 (subTreeLoop (includeVertex (fromJust (availableVertex c1)) c1)) (subTreeLoop (excludeVertex (fromJust (availableVertex c1)) c1)) else Leaf c1

-------------
-- Queries --
-------------

-- | Returns all induced subtrees in a given graph
--
-- A very short and elegant solution can be implemented using the fact that
-- 'Tree' is an instance of both 'Functor' and 'Foldable'.
--
-- >>> allSubtrees $ wheelGraph 3
-- [[],[0],[0,1],[0,2],[0,3],[1],[1,2],[1,3],[2],[2,3],[3]]
allSubtrees :: Ord v => Graph v -> [[v]]
allSubtrees g = sort $ fmap subtree (foldr (:) [] $ buildISTree g)

-- | Returns the number of induced subtrees in a given graph
--
-- >>> [numSubtrees $ wheelGraph n | n <- [3..8]]
-- [11,20,32,49,72,104]
numSubtrees :: Ord v => Graph v -> Int
numSubtrees g = length $ allSubtrees g

-- | Returns the list of all configurations with a unique id
--
-- The unique id is built by appending the letter `i` and `e` to indicate that
-- the last vertex has been either included (`i`) or excluded (`e`).
--
-- This function is useful for building a Graphviz string with unique node id.
--
-- >>> namedConfigurations "config_" $ buildISTree $ completeGraph 2
-- [("config_ii",Configuration(Included={1,2}, Excluded={}, Free={})),("config_i",Configuration(Included={1}, Excluded={}, Free={2})),("config_ie",Configuration(Included={1}, Excluded={2}, Free={})),("config_",Configuration(Included={}, Excluded={}, Free={1,2})),("config_ei",Configuration(Included={2}, Excluded={1}, Free={})),("config_e",Configuration(Included={}, Excluded={1}, Free={2})),("config_ee",Configuration(Included={}, Excluded={1,2}, Free={}))]
-- >>> map fst $ namedConfigurations "config_" $ buildISTree $ wheelGraph 3
-- ["config_ii","config_i","config_iei","config_ie","config_ieei","config_iee","config_ieee","config_","config_eii","config_ei","config_eiei","config_eie","config_eiee","config_e","config_eeii","config_eei","config_eeie","config_ee","config_eeei","config_eee","config_eeee"]
namedConfigurations :: String -> ISTree v -> [(String, Configuration v)]
namedConfigurations s (Leaf c) = [(s, c)]
namedConfigurations s (Branch c t1 t2) = (namedConfigurations (s++"i") t1)++[(s, c)]++(namedConfigurations (s++"e") t2)
--------------
-- Graphviz --
--------------

-- | Returns a Graphviz string for the given node
--
-- >>> putStrLn $ toGraphvizString "tmp/" "node_" $ buildISTree $ completeGraph 3
-- digraph {
--   node [shape=box];
--   "node_" [image="tmp/node_.png", label=""];
--   "node_i" [image="tmp/node_i.png", label=""];
--   "node_ii" [image="tmp/node_ii.png", label=""];
--   "node_i" -> "node_ii";
--   "node_ie" [image="tmp/node_ie.png", label=""];
--   "node_iei" [image="tmp/node_iei.png", label=""];
--   "node_ie" -> "node_iei";
--   "node_iee" [image="tmp/node_iee.png", label=""];
--   "node_ie" -> "node_iee";  "node_i" -> "node_ie";  "node_" -> "node_i";
--   "node_e" [image="tmp/node_e.png", label=""];
--   "node_ei" [image="tmp/node_ei.png", label=""];
--   "node_eii" [image="tmp/node_eii.png", label=""];
--   "node_ei" -> "node_eii";
--   "node_eie" [image="tmp/node_eie.png", label=""];
--   "node_ei" -> "node_eie";  "node_e" -> "node_ei";
--   "node_ee" [image="tmp/node_ee.png", label=""];
--   "node_eei" [image="tmp/node_eei.png", label=""];
--   "node_ee" -> "node_eei";
--   "node_eee" [image="tmp/node_eee.png", label=""];
--   "node_ee" -> "node_eee";  "node_e" -> "node_ee";  "node_" -> "node_e";
-- }
toGraphvizString :: (Ord v, Show v) => String -> String -> ISTree v -> String
toGraphvizString path prefix t = intercalate "\n" [headerString, toGraphvizString' path prefix t, footerString]
    where headerString = "digraph {\n  node [shape=box];"
          footerString = "}"
          nodeToGraphvizString :: String -> String -> String
          nodeToGraphvizString path nodeName = show nodeName ++ " [image=\"" ++ path ++ nodeName ++ ".png\", label=\"\"];"
          toGraphvizString' :: (Ord v, Show v) => String -> String -> ISTree v -> String
          toGraphvizString' _ _ Empty = ""
          toGraphvizString' path nodeName (Leaf _)       = "  " ++ nodeToGraphvizString path nodeName ++ "\n"
          toGraphvizString' path nodeName (Branch _ l r) = "  " ++ nodeToGraphvizString path nodeName ++ "\n"
              ++ toGraphvizString' path includedName l
              ++ "  "  ++ show nodeName ++ " -> " ++ show includedName ++ ";\n"
              ++ toGraphvizString' path excludedName r
              ++ "  "  ++ show nodeName ++ " -> " ++ show excludedName ++ ";"
              where includedName = nodeName ++ "i"
                    excludedName = nodeName ++ "e"

